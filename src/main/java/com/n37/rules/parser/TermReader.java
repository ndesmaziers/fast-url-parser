package com.n37.rules.parser;

import com.n37.rules.term.Atom;
import com.n37.rules.term.Term;
import com.n37.rules.term.Variable;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.Optional;
import java.util.Stack;

public class TermReader extends Reader {
    private final Reader in;
    private StringBuilder variableName;

    private int state = 0;

    public TermReader(Reader in) {
        this.in = in;
    }

    public Term readTerm() throws IOException {
        while (state < 5000) {
            int n = in.read();
            if(n == -1) {
                return null;
            }
            else  {
                Term term = transition((char)n);
                if(term != null) {
                    return term;
                }
                else {
                    if(state >= 5000)
                        break;
                }
            }
        }

        throw new RuntimeException();
    }

    private Term transition(char c) {
        switch (state) {
            case 0: {
                if(c == '{') {
                    state = 1;
                    return null;
                }
                else {
                    return Atom.build(c);
                }
            }
            case 1: {
                if(c == '{') {
                    state = 2;
                    variableName = new StringBuilder();
                    return null;
                }
                else {
                    state = 0;
                    return Atom.build('{');
                }
            }
            case 2: {
                if(c == '}') {
                    state = 3;
                    return null;
                }
                else {
                    variableName.append(c);
                    return null;
                }
            }
            case 3: {
                if(c == '}') {
                    state = 0;
                    return new Variable(variableName.toString());
                }
                else {
                    state = 5001;
                    return null;
                }
            }
        }
        throw new RuntimeException();
    }

    @Override
    public int read(char[] chars, int i, int i1) throws IOException {
        return in.read(chars, i, i1);
    }

    @Override
    public void close() throws IOException {
        in.close();
    }
}
