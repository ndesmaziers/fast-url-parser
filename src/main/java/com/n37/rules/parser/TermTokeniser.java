package com.n37.rules.parser;

import com.n37.rules.term.Term;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

public class TermTokeniser {

    public static List<Term> parse(String formula) throws IOException {
        var reader = new TermReader(new StringReader(formula));

        var result = new ArrayList<Term>();
        while (true) {
            var term = reader.readTerm();
            if (term != null) {
                result.add(term);
            } else {
                return result;
            }
        }
    }

    public static String toFormula(List<Term> terms) {
        StringBuilder builder = new StringBuilder();
        for (Term term : terms) {
            builder.append(term.toString());
        }
        return builder.toString();
    }
}
