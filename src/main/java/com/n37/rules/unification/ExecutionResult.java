package com.n37.rules.unification;

import com.n37.rules.term.Variable;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExecutionResult {
    private static ExecutionResult fails = new ExecutionResult(false, Collections.emptyList());
    private static ExecutionResult emptySuccess = new ExecutionResult(true, Collections.emptyList());
    private final boolean success;
    private final List<Variable> solution;

    private ExecutionResult(boolean success, List<Variable> solution) {
        this.success = success;
        this.solution = solution;
    }

    public static ExecutionResult fail() {
        return fails;
    }

    public static ExecutionResult build(boolean success) {
        return success ? emptySuccess : fails;
    }

    public boolean isSuccess() {
        return success;
    }

    public List<Variable> getSolution() {
        return solution;
    }

    public ExecutionResult with(Variable v) {
        List<Variable> copy = new ArrayList<>(solution);
        copy.add(v);
        return new ExecutionResult(this.success, copy);
    }

    public Variable getVariable(String name) {
        for (Variable var : solution) {
            if (var.getName().equals(name)) {
                return var;
            }
        }
        return null;
    }

    public void prettyPrint(PrintWriter pw) {
        pw.println(success ? "yes" : "no");
        for (Variable v : solution) {
            v.prettyPrint(pw);
            pw.println();
        }
    }
}
