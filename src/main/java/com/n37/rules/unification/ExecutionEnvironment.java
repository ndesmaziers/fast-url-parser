package com.n37.rules.unification;

import com.n37.rules.parser.TermTokeniser;
import com.n37.rules.term.Atom;
import com.n37.rules.term.Term;
import com.n37.rules.term.Variable;

import java.io.IOException;
import java.util.List;

public class ExecutionEnvironment {
    private List<Term> t1;
    private List<Term> t2;

    private int unificationLength = 0;
    private boolean _final = false;

    public ExecutionEnvironment(String formula1, String formula2) throws IOException {
        t1 = TermTokeniser.parse(formula1);
        t2 = TermTokeniser.parse(formula2);
    }

    private ExecutionEnvironment(List<Term> terms1, List<Term> terms2) {
        t1 = terms1;
        t2 = terms2;
    }

    private static List<Term> bindVariableWithList(Variable var, int length, List<Term> list) {
        var head = list.subList(0, length);
        var.bind(head);
        var tail = list.subList(length, list.size());
        return tail;
    }

    public ExecutionResult unify() {
        if (_final) {
            return ExecutionResult.fail();
        }

        if (t1.size() == 0 || t2.size() == 0) {
            _final = true;
            return ExecutionResult.build(t1.size() == t2.size());
        }

        Term left = t1.get(0);
        Term right = t2.get(0);

        if (left instanceof Atom && right instanceof Atom) {
            if (left == right) {
                ExecutionEnvironment env = new ExecutionEnvironment(t1.subList(1, t1.size()), t2.subList(1, t2.size()));
                return env.unify();
            } else {
                return ExecutionResult.fail();
            }
        } else if (left instanceof Variable && right instanceof Atom) {
            Variable leftVar = (Variable) left;
            List<Term> t1Tail = t1.subList(1, t1.size());

            while (unificationLength <= t2.size()) {
                List<Term> rightTail = bindVariableWithList(leftVar, unificationLength++, t2);
                ExecutionEnvironment env = new ExecutionEnvironment(t1Tail, rightTail);
                ExecutionResult result = env.unify();
                if (result.isSuccess()) {
                    return result.with(leftVar);
                }
            }
            return ExecutionResult.fail();
        }

        throw new RuntimeException("Not implemented yet");
    }
}
