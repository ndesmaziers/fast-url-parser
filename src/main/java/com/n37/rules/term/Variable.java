package com.n37.rules.term;

import com.n37.rules.parser.TermTokeniser;

import java.io.PrintWriter;
import java.util.List;

public class Variable implements Term {
    private String name;

    private List<Term> bounded;

    public Variable(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<Term> getBounded() {
        return bounded;
    }

    public void bind(List<Term> value) {
        this.bounded = value;
    }

    @Override
    public String toString() {
        return "Variable(" + name + ')';
    }

    public void prettyPrint(PrintWriter w) {
        w.write(name);
        w.write(" = ");
        if (bounded == null) {
            w.write('_');
        } else {
            w.write(TermTokeniser.toFormula(bounded));
        }
    }
}
