package com.n37.rules.term;

import java.util.Map;
import java.util.TreeMap;

public class Atom implements Term {
    private char value;

    public Atom(char value) {
        this.value = value;
    }

    private static Map<Character, Atom> atoms = new TreeMap<>();

    public static Atom build(Character value) {
        synchronized (atoms) {
            var found = atoms.get(value);
            if (found == null) {
                found = new Atom(value.charValue());
                atoms.put(value, found);
            }
            return found;
        }
    }

    @Override
    public String toString() {
        return Character.toString(value);
    }
}
