package com.n37.rules.unification;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

class ExecutionEnvironmentTest {

    @Test
    void emptyAtoms() throws IOException {
        doTest("", "", true);
    }

    @Test
    void t4() throws IOException {
        doTest("hello", "hello", true);
    }

    @Test
    void t2() throws IOException {
        doTest("helloz", "hello", false);
    }

    @Test
    void tx() throws IOException {
        List<ExecutionResult> res = doTest("{{v}}", "hello", true);
        // Variable v = res.getVariable("v");
    }

    @Test
    void txy() throws IOException {
        List<ExecutionResult> res = doTest("{{v1}}{{v2}}", "xy", true);
    }

    private List<ExecutionResult> doTest(String f1, String f2, boolean expected) throws IOException {
        ExecutionEnvironment env = new ExecutionEnvironment(f1, f2);

        List<ExecutionResult> result = new ArrayList<>();
        ExecutionResult res;
        do {
            res = env.unify();
            result.add(res);
        } while (res.isSuccess());

        ExecutionResult first = result.get(0);
        assertTrue(first.isSuccess() == expected);

        PrintWriter out = new PrintWriter(System.out);
        out.println("-------------------------------------------");
        out.println("'" + f1 + "' = '" + f2 + "'");

        result.stream().forEach(er -> {
            er.prettyPrint(out);
        });
        out.flush();

        return result;
    }

}