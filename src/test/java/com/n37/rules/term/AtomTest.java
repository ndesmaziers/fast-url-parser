package com.n37.rules.term;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AtomTest {

    @Test
    void build() {
        var a1 = Atom.build('a');
        var a2 = Atom.build('a');
        var b = Atom.build('b');

        assertTrue(a1 == a2);
        assertFalse(a1 == b);
    }
}