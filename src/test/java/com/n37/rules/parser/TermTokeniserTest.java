package com.n37.rules.parser;

import com.n37.rules.term.Atom;
import com.n37.rules.term.Variable;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class TermTokeniserTest {

    private Atom e = Atom.build('e');
    private Atom h = Atom.build('h');
    private Atom v = Atom.build('v');

    private Atom T = Atom.build('T');


    @Test
    void parseEmpty() throws IOException {
        var result = TermTokeniser.parse("");
        assertTrue(result.isEmpty());
    }

    @Test
    void parseAtom() throws IOException {
        var result = TermTokeniser.parse("hello");
        assertTrue(result.size() == 5);

        assertTrue(result.get(0) == h);
        assertTrue(result.get(1) == e);
    }

    @Test
    void parseWithVariable() throws IOException {
        var result = TermTokeniser.parse("This is a variable {{gamma}}, but just a brace { and an other variable {{Blue}}");

        assertTrue(result.get(0) == T);
        assertTrue(result.get(10) == v);
        assertEquals(result.get(19).toString(), "Variable(gamma)");


    }
}